import domain.pages.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.time.LocalDate;
import java.time.Month;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;

public class ChromeTest {

    private WebDriver driver;
    private WebDriverWait wait;
    private SoftAssert softAssert;

    @BeforeSuite
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeMethod
    public void setupTest() {
        driver = new ChromeDriver();
        //driver.manage().window().fullscreen();
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 3);
        softAssert = new SoftAssert();
        driver.get("http://automationpractice.com");
    }

    @AfterMethod
    public void teardown() {

        if (driver != null) {
            driver.quit();
        }

    }

    @Test
    public void testForOrderPageFactory() throws InterruptedException {
        Search search = PageFactory.initElements(driver, Search.class);
        search.doSearch("shirt");
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@title='Faded Short Sleeve T-shirts']"))).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button.exclusive"))).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title~='Proceed']"))).click();
        OrderPageFactory order = PageFactory.initElements(driver, OrderPageFactory.class);
        softAssert.assertEquals(order.getInStock(), "In stock");
        softAssert.assertEquals(order.total(), "TOTAL");
        order.clickDelete();
        softAssert.assertAll();

    }

    @Test
    public void testForOrderHtmlElements() throws InterruptedException {
        Search search = PageFactory.initElements(driver, Search.class);
        search.doSearch("shirt");
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@title='Faded Short Sleeve T-shirts']"))).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button.exclusive"))).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[title~='Proceed']"))).click();
        OrderHtmlElement order = new OrderHtmlElement(driver);
        softAssert.assertEquals(order.getInStock(), "In stock");
        softAssert.assertEquals(order.total(), "TOTAL");
        order.setQuantity("3");
        order.clickDelete();
        softAssert.assertAll();

    }

    @Test
    public void testSearch() throws InterruptedException {
        Search search = PageFactory.initElements(driver, Search.class);
        search.doSearch("shirt");
        SearchResultItem item = PageFactory.initElements(driver, SearchResultItem.class);
        softAssert.assertEquals(item.getName(), "Faded Short Sleeve T-shirts");
        softAssert.assertEquals(item.getPrice(), "$16.51");
        softAssert.assertAll();
        Thread.sleep(10000);
    }

    @Test
    public void testSearchElements() throws InterruptedException {
        Search search = PageFactory.initElements(driver, Search.class);
        search.doSearch("shirt");

        ResultItem item = new ResultItem(driver);
        softAssert.assertEquals(item.getName(), "Faded Short Sleeve T-shirts");
        softAssert.assertAll();
        Thread.sleep(10000);
    }

    @Test
    public void test() {
        softAssert.assertEquals(
                wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(
                        By.cssSelector("a.button[title='Add to cart']")))
                        .size(), 12,
                "Size not equal");
        softAssert.assertEquals(
                wait
                        .until(ExpectedConditions
                                .presenceOfAllElementsLocatedBy(
                                        By.cssSelector("a.button[title='Add to cart']")))
                        .size(), 13,
                "Size not equal");
        softAssert.assertEquals(
                wait
                        .until(ExpectedConditions
                                .presenceOfAllElementsLocatedBy(
                                        By.cssSelector("a.button[title='Add to cart']")))
                        .size(), 14,
                "Size not equal");
        softAssert.assertAll();

    }


    @Test
    public void testCreateNewAccount() throws InterruptedException {
        CreateAccountForm form = new HomePage(driver)
                .signIn()
                .createNewAccount(Math.random() + "@mfsa.ru");
        Thread.sleep(2000);
        form.clickMr();
        form.enterFN("Ksusha");
        form.enterLN("Sierikova");
        form.enterPassw("Ks1987");
        form.enterAddress("my address");
        form.enterCity("my city");
        form.setState("1");
        form.enterPostcode("12346");
        form.enterMobilePhone("1234567");
        form.enterAlias("alias");
        form.setBirthDate(LocalDate.of(1987, Month.AUGUST, 7));
        form.clickSubmit();
        softAssert.assertEquals(new HomePage(driver).isRegestered(),
                "Welcome to your account. Here you can manage all of your personal information and orders.");
        softAssert.assertAll();

    }

}
