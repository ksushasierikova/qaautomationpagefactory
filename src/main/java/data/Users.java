package data;

public enum Users {

    ADMIN("", ""),
    MANAGER("", ""),
    VASYA("", "");

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    Users(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    private String userName;
    private String password;


}
