package domain.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;

public class CreateAccountFormPO {
    private WebDriver driver;
    private WebDriverWait wait;

    public CreateAccountFormPO(WebDriver driver) {
        HtmlElementLoader.populatePageObject(this, driver);
        wait = new WebDriverWait(driver, 3);
        this.driver = driver;
    }

    public WebElement firstName() {
        return driver.findElement(By.cssSelector("#customer_firstname"));
    }

    public WebElement lastName() {
        return driver.findElement(By.cssSelector("#customer_lastname"));

    }

    public WebElement passw() {
        return driver.findElement(By.cssSelector("#passwd"));
    }

    public WebElement address() {
        return driver.findElement(By.cssSelector("#address1"));
    }

    public WebElement city() {
        return driver.findElement(By.cssSelector("#city"));
    }

    public WebElement postcode() {
        return driver.findElement(By.cssSelector("#postcode"));
    }

    public WebElement mobilePhone() {
        return driver.findElement(By.cssSelector("#phone_mobile"));
    }

    public WebElement alias() {
        return driver.findElement(By.cssSelector("#alias"));
    }

}
