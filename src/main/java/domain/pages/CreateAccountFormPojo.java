package domain.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.LinkedHashMap;
import java.util.Map;

public class CreateAccountFormPojo {
    public CreateAccountFormPojo(WebDriver driver) {
        this.driver = driver;
    }

    private WebDriver driver;

    public Map<String, String> getValue(){
        Map<String, String> fields = new LinkedHashMap<>();
        fields.put("fn", "Ksusha");
        fields.put("ln", "Sierikova");
        fields.put("pass", "Passw2018");
        fields.put("addr", "my address");
        fields.put("city", "my city");
        fields.put("pc","12346");
        fields.put("mp", "1234567");
        fields.put("al", "alias");
        return fields;
    }

    public CreateAccountFormPojo setValue(Map<String, String> fields){
        driver.findElement(By.cssSelector("#customer_firstname")).sendKeys(fields.get("fn"));
        driver.findElement(By.cssSelector("#customer_lastname")).sendKeys(fields.get("ln"));
        driver.findElement(By.cssSelector("#passwd")).sendKeys(fields.get("pass"));
        driver.findElement(By.cssSelector("#address1")).sendKeys(fields.get("addr"));
        driver.findElement(By.cssSelector("#city")).sendKeys(fields.get("city"));
        driver.findElement(By.cssSelector("#postcode")).sendKeys(fields.get("pc"));
        driver.findElement(By.cssSelector("#phone_mobile")).sendKeys(fields.get("mp"));
        driver.findElement(By.cssSelector("#alias")).sendKeys(fields.get("al"));
        return this;
    }

}
