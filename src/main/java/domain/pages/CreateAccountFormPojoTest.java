package domain.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CreateAccountFormPojoTest {
    public CreateAccountFormPojoTest(WebDriver driver) {
        this.driver = driver;
    }

    private WebDriver driver;
    private String firstName;
    private String lastName;



    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }




//    public void provideUserData(List<PojoUserData> details) {
//
//        Map<String, String> fields = new LinkedHashMap<>();
//        fields.put("", "");
//
//        for (PojoUserData user: details) {
//            driver.findElement(By.cssSelector("#id_gender1")).click();
//            driver.findElement(By.cssSelector("#customer_firstname")).sendKeys(fields.get(""));
//            driver.findElement(By.cssSelector("#customer_lastname")).sendKeys(fields.get(""));
//
//
//
//            driver.findElement(By.cssSelector("#submitAccount")).click();
//        }
//
//    }
}
