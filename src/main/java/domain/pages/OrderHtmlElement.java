package domain.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.annotations.Block;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextInput;
import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;

import static com.sun.xml.internal.ws.policy.sourcemodel.wspolicy.XmlToken.Name;

@Name("Order")
@Block(@FindBy(css = ".table.table-bordered"))
public class OrderHtmlElement extends HtmlElement {
    private WebDriver driver;
    private WebDriverWait wait;

    public OrderHtmlElement(WebDriver driver) {
        HtmlElementLoader.populatePageObject(this, driver);
        wait = new WebDriverWait(driver, 3);
    }

    @Name("Enter Quantity")
    @FindBy(css = ".cart_quantity_input.form-control.grey")
    private TextInput enterQuantity;

    public void setQuantity(String quantity){
        enterQuantity.sendKeys(quantity);
    }

    @Name("Delete button")
    @FindBy(css = ".icon-trash")
    private Button deleteIcon;

    public void clickDelete() {
        deleteIcon.click();
    }

    @Name("In Stock")
    @FindBy(css = ".label.label-success")
    public WebElement getInStock;

    public String getInStock() {
        return getInStock.getText();
    }

    @Name("TOTAL")
    @FindBy(css = ".total_price_container.text-right")
    public WebElement total;

    public String total() {
        return total.getText();
    }


}
