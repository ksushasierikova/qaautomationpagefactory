package domain.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OrderPageFactory {

    private WebDriver driver;
    private WebDriverWait wait;

    public OrderPageFactory(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 3);
    }

    @FindBy(css = ".table.table-bordered .icon-trash")
    private WebElement deleteIcon;

    public void clickDelete(){
        deleteIcon.click();
    }

    @FindBy(css=".table.table-bordered .label.label-success")
    public WebElement getInStock;
    public String getInStock(){
        return getInStock.getText();
    }

    @FindBy(css=".table.table-bordered .total_price_container.text-right")
    public WebElement total;
    public String total(){
        return total.getText();
    }

}
