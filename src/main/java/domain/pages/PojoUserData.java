package domain.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class PojoUserData {

    private String firstName;
    private String lastName;

    public PojoUserData(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

//    public PojoUserData setLastName(){
//
//        return this;
//    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

//    public static Map<String, String> getLastName() {
//        Map<String, String> fields = new LinkedHashMap<>();
//        fields.put("", "");
//        return fields;
//    }
}
