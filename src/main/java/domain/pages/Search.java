package domain.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Search {
    private WebDriver driver;

    public Search(WebDriver driver){
        this.driver=driver;

    }

    @FindBy(css="input.search_query")
    private WebElement searchInput;

    @FindBy(css="button.button-search")
    private WebElement searchSubmit;

    public void doSearch(String query){
        searchInput.sendKeys(query);
        searchInput.click();
        Actions action = new Actions(driver);
        action.keyDown(Keys.CONTROL).sendKeys("A").keyUp(Keys.CONTROL)
                .build().perform();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        searchSubmit.click();
    }
}
