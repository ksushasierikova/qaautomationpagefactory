package domain.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SearchResultItem {
    private WebDriver driver;
    private WebDriverWait wait;

    public SearchResultItem(WebDriver driver){
        this.driver=driver;
        wait = new WebDriverWait(driver, 3);
    }

    @FindBy(css=".product_list .product-price")
    private List<WebElement> price;

    @FindBy(css=".product_list a.product-name")
    private List<WebElement> names;

    @FindBy(css=".product_list a.product-name")
    public WebElement name;

    public String getPrice(){
        return price.get(1).getText();
    }

    public String getName(){
        return wait.until(ExpectedConditions.visibilityOf(name)).getText();
    }
}
