package tests;

import domain.pages.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import utils.TestListener;

import java.time.LocalDate;
import java.time.Month;
import java.util.concurrent.TimeUnit;

@Listeners(TestListener.class)
public class ChromeTest {

    private WebDriver driver;
    private WebDriverWait wait;
    private SoftAssert softAssert;

    public WebDriver getDriver() {
        return driver;
    }

    @BeforeSuite
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
        Reporter.log("Before Suit executed", 1, true);
    }

    @BeforeMethod
    public void setupTest() {
        driver = new ChromeDriver();
        //driver.manage().window().fullscreen();
        driver.manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 3);
        softAssert = new SoftAssert();
        driver.get("http://automationpractice.com");
        Reporter.log("Before Method executed", 1, true);
    }

    @AfterMethod
    public void teardown() {
        Reporter.log("After Method executed", 1, true);
        if (driver != null) {
            driver.quit();
        }

    }

    @Test
    public void testCreateAccountPojoFirst() throws InterruptedException {
        CreateAccountForm form = new HomePage(driver)
                .signIn()
                .createNewAccount(Math.random() + "@mfsa.ru");
        Thread.sleep(2000);
        CreateAccountPojo formPojo = new CreateAccountPojo();
        driver.findElement(By.cssSelector("#customer_firstname")).sendKeys(formPojo.getLastName());
        driver.findElement(By.cssSelector("#customer_lastname")).sendKeys(formPojo.getLastName());
        driver.findElement(By.cssSelector("#passwd")).sendKeys(formPojo.getPassword());
        driver.findElement(By.cssSelector("#address1")).sendKeys(formPojo.getAddress());
        driver.findElement(By.cssSelector("#city")).sendKeys(formPojo.getCity());
        driver.findElement(By.cssSelector("#postcode")).sendKeys(formPojo.getPostCode());
        driver.findElement(By.cssSelector("#phone_mobile")).sendKeys(formPojo.getMobilePhone());
        driver.findElement(By.cssSelector("#alias")).sendKeys(formPojo.getAlias());
        form.clickMr();
        form.setState("1");
        form.setBirthDate(LocalDate.of(1987, Month.AUGUST, 7));
        form.clickSubmit();
        softAssert.assertEquals(new HomePage(driver).isRegestered(),
                "Welcome to your account. Here you can manage all of your personal information and orders.");
        softAssert.assertAll();

    }

    @Test
    public void testCreateAccountPojoSecond() throws InterruptedException {
        CreateAccountForm form = new HomePage(driver)
                .signIn()
                .createNewAccount(Math.random() + "@mfsa.ru");
        Thread.sleep(2000);
        CreateAccountPojo formPojo = new CreateAccountPojo();
        CreateAccountFormPO createAccountFormPO = new CreateAccountFormPO(driver);
        createAccountFormPO.firstName().sendKeys(formPojo.getFirstName());
        createAccountFormPO.lastName().sendKeys(formPojo.getLastName());
        createAccountFormPO.passw().sendKeys(formPojo.getPassword());
        createAccountFormPO.address().sendKeys(formPojo.getAddress());
        createAccountFormPO.city().sendKeys(formPojo.getCity());
        createAccountFormPO.postcode().sendKeys(formPojo.getPostCode());
        createAccountFormPO.mobilePhone().sendKeys(formPojo.getMobilePhone());
        createAccountFormPO.alias().sendKeys(formPojo.getAlias());
        form.clickMr();
        form.setState("1");
        form.setBirthDate(LocalDate.of(1987, Month.AUGUST, 7));
        form.clickSubmit();
        softAssert.assertEquals(new HomePage(driver).isRegestered(),
                "Welcome to your account. Here you can manage all of your personal information and orders.");
        softAssert.assertAll();

    }


    @Test
    public void testCreateNewAccountPojo() throws InterruptedException {
        CreateAccountForm form = new HomePage(driver)
                .signIn()
                .createNewAccount(Math.random() + "@mfsa.ru");
        Thread.sleep(2000);
        CreateAccountFormPojo formPojo = new CreateAccountFormPojo(driver);
        formPojo.setValue(formPojo.getValue());
        form.clickMr();
        form.setState("1");
        form.setBirthDate(LocalDate.of(1987, Month.AUGUST, 7));
        form.clickSubmit();
        softAssert.assertEquals(new HomePage(driver).isRegestered(),
                "Welcome to your account. Here you can manage all of your personal information and orders.");
        softAssert.assertAll();

    }

    @Test(groups = "regression")
    public void testSearch() throws InterruptedException {
        Search search = PageFactory.initElements(driver, Search.class);
        search.doSearch("shirt");
        SearchResultItem item = PageFactory.initElements(driver, SearchResultItem.class);
        softAssert.assertEquals(item.getName(), "Faded Short Sleeve T-shirts");
        softAssert.assertEquals(item.getPrice(), "$16.51");
        softAssert.assertAll();

        Thread.sleep(10000);
    }

    @Test
    public void hoverTest() {
        new Actions(driver).moveToElement(wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".shopping_cart")))).build().perform();
        softAssert.assertTrue(wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#button_order_cart"))).isDisplayed());
    }

    @Test
    public void testCreateNewAccount() throws InterruptedException {
        CreateAccountForm form = new HomePage(driver)
                .signIn()
                .createNewAccount(Math.random() + "@mfsa.ru");

        form.setBirthDate(LocalDate.now());

    }

    @Test
    public void testSearchElements() throws InterruptedException {
        Search search = PageFactory.initElements(driver, Search.class);
        search.doSearch("shirt");
        ResultItem item = new ResultItem(driver);
        softAssert.assertTrue(false);
        softAssert.assertEquals(item.getName(), "Faded Short Sleeve T-shirts");
        softAssert.assertAll();
        Thread.sleep(10000);
    }


    @Test
    public void test() {
        softAssert.assertEquals(
                wait
                        .until(ExpectedConditions
                                .presenceOfAllElementsLocatedBy(
                                        By.cssSelector("a.button[title='Add to cart']")))
                        .size(), 12,
                "Size not equal");
        softAssert.assertEquals(
                wait
                        .until(ExpectedConditions
                                .presenceOfAllElementsLocatedBy(
                                        By.cssSelector("a.button[title='Add to cart']")))
                        .size(), 13,
                "Size not equal");
        softAssert.assertEquals(
                wait
                        .until(ExpectedConditions
                                .presenceOfAllElementsLocatedBy(
                                        By.cssSelector("a.button[title='Add to cart']")))
                        .size(), 14,
                "Size not equal");
        softAssert.assertAll();


    }

    @Test
    public void testCreateNewAccount2() throws InterruptedException {
        CreateAccountForm form = new HomePage(driver)
                .signIn()
                .createNewAccount(Math.random() + "@mfsa.ru");
        Thread.sleep(2000);
        form.clickMr();
        form.enterFN("Ksusha");
        form.enterLN("Sierikova");
        form.enterPassw("Ks1987");
        form.enterAddress("my address");
        form.enterCity("my city");
        form.setState("1");
        form.enterPostcode("12346");
        form.enterMobilePhone("1234567");
        form.enterAlias("alias");
        form.setBirthDate(LocalDate.of(1987, Month.AUGUST, 7));
        form.clickSubmit();
        softAssert.assertEquals(new HomePage(driver).isRegestered(),
                "Welcome to your account. Here you can manage all of your personal information and orders.");
        softAssert.assertAll();

    }

    @Test
    public void testSearchElements2() throws InterruptedException {
        Search search = PageFactory.initElements(driver, Search.class);
        search.doSearch("shirt");
        ResultItem item = new ResultItem(driver);
        softAssert.assertEquals(item.getName(), "Faded Short Sleeve T-shirts");
        softAssert.assertAll();
        Thread.sleep(10000);
    }


}
