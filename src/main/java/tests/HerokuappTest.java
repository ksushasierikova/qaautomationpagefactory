package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class HerokuappTest {


    private WebDriver driver;
    private WebDriverWait wait;
    private SoftAssert softAssert;

    public WebDriver getDriver() {
        return driver;
    }

    @BeforeSuite
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
        Reporter.log("Before Suit executed", 1, true);
    }

    @BeforeMethod
    public void setupTest() {
        driver = new ChromeDriver();
        //driver.manage().window().fullscreen();
        driver.manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 3);
        softAssert = new SoftAssert();
        //driver.get("http://the-internet.herokuapp.com/tables");
        driver.get("http://the-internet.herokuapp.com/upload");
        //http://the-internet.herokuapp.com/upload
        Reporter.log("Before Method executed", 1, true);
    }

    @AfterMethod
    public void teardown() {
        Reporter.log("After Method executed", 1, true);
        if (driver != null) {
            driver.quit();
        }

    }

    @Test
    public void fileUploader() throws AWTException {
        StringSelection ss = new StringSelection(new File("./src/main/resources/filesToUpload/photo.jpg").getAbsolutePath());
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

        driver.findElement(By.cssSelector("#file-upload")).click();
        Robot robot = new Robot();
        // Ctrl-V + Enter on Win
        robot.delay(3000);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);

        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#file-submit"))).click();
        softAssert.assertEquals(driver.findElement(By.xpath("//*[@class = 'example']/h3")).getText(), "File Uploaded!");
        softAssert.assertEquals(driver.findElement(By.cssSelector("#uploaded-files")).getText(), "photo.jpg");
        softAssert.assertAll();

    }

    @Test
    public void dataTables() {

        WebElement headerLN = driver.findElement(By.xpath("//*[@class='header headerSortUp']"));
        WebElement firstElement = driver.findElement(By.xpath("(//*[@id = 'table1']/tbody/tr/td)[1]"));
        softAssert.assertEquals(firstElement.getText(), "Smith");
        headerLN.click();
        WebElement forthElement = driver.findElement(By.xpath("(//*[@id = 'table1']/tbody/tr/td)[18]"));
        softAssert.assertEquals(forthElement.getText(), "Smith");
        softAssert.assertAll();

    }

    @Test(dataProvider = "tableData")
    public void testTable(String colName, List colData) {
        List<WebElement> TableHeaders = driver.findElements(By.cssSelector("#table1 .header"));
        List<WebElement> tableRows = driver.findElements(By.cssSelector("#table1 tbody tr"));

        int colIndex = TableHeaders.stream().map(WebElement::getText).collect(Collectors.toList()).indexOf(colName);
        TableHeaders.get(colIndex).click();
        List<String> actualRows = tableRows.stream()
                .map(i -> i.findElements(By.cssSelector("td"))
                        .get(colIndex)).map(WebElement::getText)
                .peek(System.out::println)
                .collect(Collectors.toList());


    }

    @DataProvider(name = "tableData")
    public Object[][] provideSomeData() {
        List expectedLastNames = new ArrayList<String>();
        List expectedFirstNames = new ArrayList<String>();
        List expectedEmails = new ArrayList<String>();
        List expectedDues = new ArrayList<String>();
        List expectedWebSites = new ArrayList<String>();
        expectedLastNames.add("Bach");
        expectedLastNames.add("Doe");
        expectedLastNames.add("Smith");
        expectedLastNames.add("Conway");

        return new Object[][]{
                {"Last Name", expectedLastNames},
                {"First Name", expectedFirstNames},
                {"Email", expectedEmails},
                {"Due", expectedDues},
                {"Web Site", expectedWebSites},

        };

    }
}
