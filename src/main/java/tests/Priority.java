package tests;

import data.Users;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

public class Priority {

    @Test(priority = 1)
    public void test1(){
        System.out.println("pr 1");
    }

    @Ignore //test will be ignored
    @Test(priority = 2)
    public void test2(){
        System.out.println("pr 2");
    }

    @Test(priority = 3) //самый меньший приоритет
    public void test3(){
        System.out.println("pr 3");
    }

    @Test
    public void testEnum(){
        System.out.println(Users.ADMIN.getPassword());
    }

}
