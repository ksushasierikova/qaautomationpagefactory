package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.concurrent.TimeUnit;

public class TestDragAndDrop {

    private WebDriver driver;
    private WebDriverWait wait;
    private SoftAssert softAssert;

    public WebDriver getDriver() {
        return driver;
    }

    @BeforeSuite
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
        Reporter.log("Before Suit executed", 1, true);
    }

    @BeforeMethod
    public void setupTest() {
        driver = new ChromeDriver();
        //driver.manage().window().fullscreen();
        driver.manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 3);
        softAssert = new SoftAssert();
        driver.get("http://the-internet.herokuapp.com/drag_and_drop");
        Reporter.log("Before Method executed", 1, true);
    }

    @AfterMethod
    public void teardown() {
        Reporter.log("After Method executed", 1, true);
        if (driver != null) {
            driver.quit();
        }

    }

    @Test
    public void dragAndDrop() {
        WebElement elementA = driver.findElement(By.cssSelector("#column-a"));
        WebElement elementB = driver.findElement(By.cssSelector("#column-b"));
        Actions actions = new Actions(driver);
        Point locationA = elementA.getLocation();
        Point locationB = elementB.getLocation();
        Dimension getSizeA = elementA.getSize();

        Dimension getSizeB = elementB.getSize();

//        Point locationA = (40, 98);
//        Point locationB = (260, 98);

        //actions.dragAndDrop(elementA, elementB).perform();
        actions.dragAndDropBy(elementA, locationA.x+getSizeB.height/2, locationB.y+getSizeB.width/2)
                .build().perform();
        softAssert.assertEquals(locationB, locationA);
        softAssert.assertAll();

    }

}
//Point locationA = (40;98);
//Point locationA = (260; 98);