package tests;

import org.testng.annotations.*;
import utils.TestListener;

import java.lang.reflect.Array;
import java.util.Arrays;

//@Listeners(value = (TestListener.class))
public class UnitTest {

    private String driver = "driver";

    @Test(priority = 1, dependsOnMethods = "testDataProvider") //priority 0 - последним выполненение будет
    public void test1() {
        System.out.println("test1");
    }


    @Test(dataProvider = "provideSomeData", threadPoolSize = 2, invocationCount = 100)
    // test will be run 100 times in two threads
    public void testDataProvider(String test, boolean condition) {
        System.out.println(test);
        assert condition;

    }

    @Parameters({"param1", "param2"})
    @Test(parameters = "param")
    public void testParamentes(@Optional String param1, String param2) { //@Optional = null
        System.out.println(param1 + param2);
    }

    @DataProvider(name = "provideSomeData", parallel = true)
    public Object[][] createData1() {
        Object[][] i = new Object[10][2];// 10 - сколько параметров, 2 - сколько раз запускать тест
        Arrays.stream(i).forEach(objects -> Arrays.stream(objects).forEach(n -> n = 'f'));

//        for (int e = 0; e< 10; e++)
//                for(int f = 0; f< 2; f++){
//            i[e][f] = 'f';
//                }

//        return new Object[][] {
//                { "Cedric", true },
//                { "Anne", false},
//        };
        return i;
    }


}
