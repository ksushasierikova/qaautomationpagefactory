package utils;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import tests.ChromeTest;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TestListener implements ITestListener {
    @Override
    public void onTestStart(ITestResult result) {
        System.out.println("Test Start on "+result.getStartMillis());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("Test Success");
        System.out.println("Test Success on "+result.getEndMillis());
        System.out.println("Test took"+(result.getEndMillis()-result.getStartMillis()));
    }

    @Override
    public void onTestFailure(ITestResult result) {
        WebDriver webDriver= ((ChromeTest)result.getInstance()).getDriver();
        if(webDriver!=null) {
            Screenshot myScreenshot  = new AShot()
                    .shootingStrategy(ShootingStrategies.viewportPasting(100))
                    .takeScreenshot(webDriver);
            File outputfile = new File(result.getName()+"_"+ LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH_mm_ss"))+"image.jpg");
            try {
                ImageIO.write(myScreenshot.getImage(), "jpg", outputfile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Test Failure on "+result.getName());

    }

    @Override
    public void onTestSkipped(ITestResult result) {
        System.out.println("Test Skipped");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {
        System.out.println("Start");
    }

    @Override
    public void onFinish(ITestContext context) {
        System.out.println("Finish");
    }
}
